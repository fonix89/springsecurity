package com.mapsa.SpringSecurityWithThymeleaf.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "USER_TBL")

public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank(message = "Must not Empty")
    private String name;

    //Email in this project use as a password.
    @NotBlank
    //@Email(message = "Invalid Format")
    private String email;

    @Enumerated(value = EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles = new ArrayList<>();
}
