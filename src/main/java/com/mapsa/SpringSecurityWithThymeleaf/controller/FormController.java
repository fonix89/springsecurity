package com.mapsa.SpringSecurityWithThymeleaf.controller;

import com.mapsa.SpringSecurityWithThymeleaf.domain.User;
import com.mapsa.SpringSecurityWithThymeleaf.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class FormController {
    private final UserService userService;

    @GetMapping(value = "/register")
    public String form(User user) {
        return "newuser";
    }

    @PostMapping(value = "/register")
    public String register(@Valid User user, BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return "newuser";
        }

        userService.add(user);
        System.out.println(user.toString());
        return "redirect:/";
    }

    @GetMapping(value = "/")
    public String index(Model model) {
        model.addAttribute("userslist", userService.findAll());
        return "index";
    }

    @GetMapping(value = "/delete/{id}")
    public String delete(@PathVariable Integer id) {
        userService.deleteById(id);
        return "redirect:/";
    }

    @ResponseBody
    @PreAuthorize(value = "hasAuthority('USER')")
    @GetMapping(value = "/hello")
    public String sayHello(){
        return "hello user.";
    }

    @ResponseBody
    @PreAuthorize(value = "hasAuthority('ADMIN')")
    @GetMapping(value = "/helloA")
    public String sayHelloToAdmin(){
        return "hello admin.";
    }

    @ResponseBody
    @GetMapping(value = "/getUser")
    public String getCurrentUser(){
     return userService.getCurrentUser().toString();
    }


}
