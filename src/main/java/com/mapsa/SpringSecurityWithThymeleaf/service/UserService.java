package com.mapsa.SpringSecurityWithThymeleaf.service;

import com.mapsa.SpringSecurityWithThymeleaf.domain.User;

import java.util.List;

public interface UserService {

    void add(User user);

    List<User> findAll();

    void deleteById(Integer userId);

    User getCurrentUser();
}
