package com.mapsa.SpringSecurityWithThymeleaf.service;


import com.mapsa.SpringSecurityWithThymeleaf.repo.UserRepository;
import com.mapsa.SpringSecurityWithThymeleaf.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    @Override
    public UserPrincipal loadUserByUsername(String s) throws UsernameNotFoundException {
        return new UserPrincipal(userRepository.findByName(s));
    }
}