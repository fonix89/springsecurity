package com.mapsa.SpringSecurityWithThymeleaf.service;

import com.mapsa.SpringSecurityWithThymeleaf.domain.Role;
import com.mapsa.SpringSecurityWithThymeleaf.domain.User;
import com.mapsa.SpringSecurityWithThymeleaf.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImp implements UserService, CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void add(User user) {
        user.setEmail(passwordEncoder.encode(user.getEmail()));
        user.getRoles().add(Role.USER);
        userRepository.save(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public void deleteById(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = new User();
        user.setName("Hamed");
        user.setEmail(passwordEncoder.encode("hamed@info.com"));
        user.setRoles(Collections.unmodifiableList(Arrays.asList(Role.ADMIN, Role.USER)));
        userRepository.save(user);
    }

    public User getCurrentUser(){
       Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(object);
        return null;
    }
}
