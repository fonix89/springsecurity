package com.mapsa.SpringSecurityWithThymeleaf.security;

import org.springframework.security.core.userdetails.User;

public class UserPrincipal extends User {

    public UserPrincipal(com.mapsa.SpringSecurityWithThymeleaf.domain.User user) {
        super(user.getName(), user.getEmail(), user.getRoles());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
